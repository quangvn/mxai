#!/bin/bash

bot[0]="Machete"
bot[1]="Jay_Bot"
bot[2]="MctsAi"
bot[3]="Ni1mir4ri"
bot[4]="RatioBot"

for i in `seq 0 $2`;
do
	echo ${bot[i%5]}
	cp data/aiData/hm.txt data/aiData/backup/hm_${i}.txt
	if [ ${i%10} = 0 ]; then
		echo data/aiData/backup/hm_${i}.txt
	fi
	java -ea -cp FightingICE.jar:./lib/fileLib.jar:./lib/gameLib.jar:./lib/javatuples-1.2.jar:./lib/commons_csv.jar:./lib/jinput.jar:./lib/lwjgl_util.jar:./lib/lwjgl.jar:./lib/Jama-1.0.3.jar -Djava.library.path="./lib/native/linux" Main -n 1 --c1 ZEN --c2 ZEN --a1 ${bot[i%5]} --a2 $1
	sleep 5
	java -ea -cp FightingICE.jar:./lib/fileLib.jar:./lib/gameLib.jar:./lib/javatuples-1.2.jar:./lib/commons_csv.jar:./lib/jinput.jar:./lib/lwjgl_util.jar:./lib/lwjgl.jar:./lib/Jama-1.0.3.jar -Djava.library.path="./lib/native/linux" Main -n 1 --c1 ZEN --c2 ZEN --a1 ${bot[i%5]} --a2 $1
	sleep 5
done