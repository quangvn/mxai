package memory;

import java.util.List;

import enumerate.Action;
import structs.FrameData;

public interface MemoryInterface {
	/**
	 * store memory to file
	 */
	public void store();

	/**
	 * load memory from file
	 */
	public void load();

	/**
	 * retrieve most used actions in current state
	 */
	public List<Action> getFreqActions(FrameData fData, boolean player, int noActs);

	/**
	 * retrieve actions with best stats
	 */
	public List<Action> getBestActions(FrameData fData, boolean player, int noActs);
}
