package memory;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.IntStream;

import enumerate.Action;
import memory.layer.DirLayer;
import memory.layer.DistanceLayer;
import memory.layer.ManaLayer;
import memory.layer.ProjectileLayer;
import memory.layer.XPosLayer;
import memory.layer.YPosLayer;
import structs.CharacterData;
import structs.FrameData;

class StateCode {
	int myPrj;
	int oppPrj;
	int distance;
	int myEnergy;
	int oppEnergy;
	int xPos;
	int yPos;
	int oppYPos;
	int dir;

	static ProjectileLayer prjLayer = new ProjectileLayer();
	static ManaLayer manaLayer = new ManaLayer();
	static XPosLayer xPosLayer = new XPosLayer();
	static YPosLayer yPosLayer = new YPosLayer();
	static DirLayer dirLayer = new DirLayer();
	static DistanceLayer distLayer = new DistanceLayer();

	static StateCode encode(FrameData fData, boolean player) {
		StateCode state = new StateCode();
		CharacterData myChar = fData.getMyCharacter(player);
		CharacterData oppChar = fData.getOpponentCharacter(player);

		state.myPrj = prjLayer.getCode(fData, player); // 4
		state.oppPrj = prjLayer.getCode(fData, !player); // 4
		state.distance = distLayer.classify(Math.abs(myChar.left - oppChar.left)); // 6
		state.myEnergy = manaLayer.classify(myChar.energy); // 8
		state.oppEnergy = manaLayer.classify(oppChar.energy); // 8
		state.xPos = xPosLayer.classify(myChar.left); // 5
		state.yPos = yPosLayer.classify(myChar.top); // 6
		state.oppYPos = yPosLayer.classify(oppChar.top); // 6
		state.dir = dirLayer.classify(myChar.left - oppChar.left); // 2

		return state;
	}

	@Override
	public int hashCode() {
		return (myPrj*1317458 + oppPrj*232697 + distance*19897 + myEnergy*9997
				+ oppEnergy*997 + xPos*97 + yPos*47 + oppYPos*23 + dir) % 20189069;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
      return false;
		}

		if (!StateCode.class.isAssignableFrom(obj.getClass())) {
      return false;
		}
		final StateCode code = (StateCode) obj;

		if (this.myPrj != code.myPrj) return false;
		if (this.oppPrj != code.oppPrj) return false;
		if (this.distance != code.distance) return false;
		if (this.myEnergy != code.myEnergy) return false;
		if (this.oppEnergy != code.oppEnergy) return false;
		if (this.xPos != code.xPos) return false;
		if (this.yPos != code.yPos) return false;
		if (this.oppYPos != code.oppYPos) return false;
		if (this.dir != code.dir) return false;

		return true;
	}
}

class ActionStats {
	double[] score;
	int[] testCount;
	double[] ucbs;
	int totalVisit;

	static ActionStats generateBlankStats() {
		ActionStats stats = new ActionStats();
		stats.updateUCB();
		return stats;
	}

	ActionStats() {
		totalVisit = 1;
		score = new double[HashMemory.actionCount];
		testCount = new int[HashMemory.actionCount];
		ucbs = new double[HashMemory.actionCount];
	}

	void updateUCB() {
		for (int i=0; i<HashMemory.actionCount; i++) {
			if (testCount[i] < 10) {
				ucbs[i] = 99999 + HashMemory.rand.nextInt(100);
			} else {
				ucbs[i] = score[i]/testCount[i] + 10.0*Math.sqrt(Math.log(totalVisit)/testCount[i]);
			}
		}
	}

	void rescale() {
		this.totalVisit /= 4;
		for (int i=0; i<HashMemory.actionCount; i++) {
			testCount[i] /= 4;
			score[i] /= 4;
		}
	}
}

public class HashMemory implements MemoryInterface {
	static Random rand = new Random();
	static Action[] actions = Action.values();
	static int actionCount = actions.length;
	static String DATA_FILE = "data/aiData/hm.txt";

	HashMap<StateCode, ActionStats> hash;

	public HashMemory() {
		hash = new HashMap<>(50000);
	}

	@Override
	public void store() {
		try {
			PrintWriter pw = new PrintWriter(new BufferedWriter (new FileWriter(DATA_FILE, false)));
			pw.println(hash.size());

			for (Map.Entry<StateCode, ActionStats> entry : hash.entrySet()) {
				StateCode code = entry.getKey();
				pw.print(code.myPrj + " ");
				pw.print(code.oppPrj + " ");
				pw.print(code.distance + " ");
				pw.print(code.myEnergy + " ");
				pw.print(code.oppEnergy + " ");
				pw.print(code.xPos + " ");
				pw.print(code.yPos + " ");
				pw.print(code.oppYPos + " ");
				pw.println(code.dir + " ");

				ActionStats stats = entry.getValue();

				if (stats.totalVisit > 8000) {
					stats.rescale();
				}

				pw.println(stats.totalVisit);
				for (int i=0; i<actionCount; i++) {
					pw.print(stats.testCount[i] + " ");
				}
				pw.println();
				for (int i=0; i<actionCount; i++) {
					pw.print(stats.score[i] + " ");
				}
				pw.println();
			}

			pw.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	@Override
	public void load() {
		File file = new File(HashMemory.DATA_FILE);
		BufferedReader br;

		try {
			br = new BufferedReader(new FileReader(file));
			int count = Integer.valueOf(br.readLine());

			for (; count > 0; count--) {
				StateCode code = new StateCode();

				String[] arr = br.readLine().split(" ");
				code.myPrj = Integer.valueOf(arr[0]);
				code.oppPrj = Integer.valueOf(arr[1]);
				code.distance = Integer.valueOf(arr[2]);
				code.myEnergy = Integer.valueOf(arr[3]);
				code.oppEnergy = Integer.valueOf(arr[4]);
				code.xPos = Integer.valueOf(arr[5]);
				code.yPos = Integer.valueOf(arr[6]);
				code.oppYPos = Integer.valueOf(arr[7]);
				code.dir = Integer.valueOf(arr[8]);

				ActionStats stats = new ActionStats();
				stats.totalVisit = Integer.valueOf(br.readLine());
				arr = br.readLine().split(" ");
				for (int i=0; i<actionCount; i++) {
					stats.testCount[i] = Integer.valueOf(arr[i]);
				}
				arr = br.readLine().split(" ");
				assert(arr.length == actionCount);
				for (int i=0; i<actionCount; i++) {
					stats.score[i] = Double.valueOf(arr[i]);
				}

				stats.updateUCB();
				hash.put(code, stats);
			}

			br.close();
		} catch (IOException e) {
			if (!file.exists()) return;
			for (int i=0; i<1000000; i++) {i=i;}
			System.out.println("RELOAD ----- RELOAD ---- RELOAD");
			this.load();
		}
	}

	@Override
	public List<Action> getFreqActions(FrameData fData, boolean player, int noActs) {
		return null;
	}

	@Override
	public List<Action> getBestActions(FrameData fData, boolean player, int noActs) {
		StateCode code = StateCode.encode(fData, player);
		ActionStats stats = hash.getOrDefault(code, ActionStats.generateBlankStats());

		int[] sortedActions = IntStream.range(0, stats.ucbs.length)
        .boxed().sorted((i, j) -> Double.compare(stats.ucbs[j], stats.ucbs[i]) )
        .mapToInt(ele -> ele).toArray();

		List<Action> res = new ArrayList<Action>();
		for (int i=0; i<noActs; i++) {
			res.add(actions[sortedActions[i]]);
			// System.out.print(stats.ucbs[sortedActions[i]] + " ");
		}
		// System.out.println();

		return res;
	}

	public List<Action> getBestActions2(FrameData fData, boolean player, int noActs) {
		StateCode code = StateCode.encode(fData, player);
		ActionStats stats = hash.getOrDefault(code, ActionStats.generateBlankStats());

		int[] sortedActions = IntStream.range(0, stats.ucbs.length)
        .boxed().sorted((i, j) -> Double.compare(stats.ucbs[j], stats.ucbs[i]) )
        .mapToInt(ele -> ele).toArray();

		List<Action> res = new ArrayList<Action>();
		for (int i=0; i<noActs; i++) {
			res.add(actions[sortedActions[i]]);
		}

		return res;
	}

	// TODO this function has bug, probably uninitialized testCount
	public double getBestActionScore(FrameData fData, boolean player) {
		StateCode code = StateCode.encode(fData, player);

		double res = -99999;
		if (hash.containsKey(code)) {
			ActionStats stats = hash.get(code);
			for (int i=0; i<actionCount; i++) {
				if (stats.score[i]/stats.testCount[i] > res) {
					res = stats.score[i]/stats.testCount[i];
				}
			}

			return res;
		}
		return 0;
	}

	public void batchUpdate(FrameData fData, boolean player, int[] act, double[] score) {
		StateCode code = StateCode.encode(fData, player);
		ActionStats stats = hash.getOrDefault(code, new ActionStats());
		for (int i=0; i<act.length; i++) {
			stats.score[act[i]] += score[i];
			stats.testCount[act[i]] ++;
		}
		stats.totalVisit ++;
		stats.updateUCB();
		hash.put(code, stats);
	}

}
