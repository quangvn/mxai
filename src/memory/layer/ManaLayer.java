package memory.layer;

import structs.FrameData;

public class ManaLayer extends MemoryLayer {
	private static int scale[] = {30, 40, 50};
	@Override
	public int getCode(FrameData fData, boolean player) {
		return classify(fData.getOpponentCharacter(player).getEnergy());
	}

	@Override
	public int classify(int mana) {
		int res = 0;
		while (mana >= scale[res]) {
			res ++;
			if (res == scale.length) break;
		}
		return res;
	}

	@Override
	public int getRange() {
		return scale.length+1;
	}

}
