package memory.layer;

import structs.FrameData;

public class YPosLayer extends MemoryLayer {
	// 0 - 640 size 140
	private static int[] scale = {440, 420, 400, 200}; // CROUCH, STAND, AIR
	@Override
	public int getCode(FrameData fData, boolean player) {
		return classify(fData.getMyCharacter(player).top);
	}

	@Override
	public int classify(int x) {
		int res = 0;
		while (x < scale[res]) {
			res ++;
			if (res == scale.length) break;
		}
		return res;
	}

	@Override
	public int getRange() {
		return scale.length+1;
	}
}
