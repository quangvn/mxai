package memory.layer;

import structs.FrameData;

public class XPosLayer extends MemoryLayer {
	// 0 - 960 size 20
	private static int[] scale = {100, 300, 640, 840};
	@Override
	public int getCode(FrameData fData, boolean player) {
		return classify(fData.getMyCharacter(player).left);
	}

	@Override
	public int classify(int hp) {
		int res = 0;
		while (hp > scale[res]) {
			res ++;
			if (res == scale.length) break;
		}
		return res;
	}

	@Override
	public int getRange() {
		return scale.length+1;
	}
}
