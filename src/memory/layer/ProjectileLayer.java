package memory.layer;

import java.util.Deque;

import fighting.Attack;
import structs.FrameData;

public class ProjectileLayer extends MemoryLayer{
	@Override
	public int getCode(FrameData fData, boolean player) {
		int res = 0;
		Deque<Attack> prj = fData.getAttack();
		for (Attack a : prj) {
			if (a.isPlayerNumber() == player) {
				res ++;
			}
		}
		return classify(res);
	}

	// 0 1 2
	@Override
	public int classify(int c) {
		if (c < 2) return c;
		return 2;
	}

	@Override
	public int getRange() {
		return 3;
	}
}
