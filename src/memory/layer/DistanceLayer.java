package memory.layer;

import structs.FrameData;

public class DistanceLayer extends MemoryLayer {
	private static int[] scale = {80, 120, 180, 200, 350};

	@Override
	public int getCode(FrameData fData, boolean player) {
		return classify(Math.abs(fData.getP1().left - fData.getP2().left));
	}

	@Override
	public int classify(int data) {
		int res = 0;
		while (data > scale[res]) {
			res++;
			if (res == scale.length) break;
		}
		return res;
	}

	@Override
	public int getRange() {
		return scale.length+1;
	}
}
