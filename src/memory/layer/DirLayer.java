package memory.layer;

import structs.FrameData;

/**
 * classify whether the opponent is on the right or left
 * @author perya
 *
 */
public class DirLayer extends MemoryLayer {
	@Override
	public int getCode(FrameData fData, boolean player) {
		return classify(fData.getMyCharacter(player).getLeft() - fData.getOpponentCharacter(player).getLeft());
	}

	@Override
	public int classify(int data) {
		if (data > 0) return 1;
		return 0;
	}

	@Override
	public int getRange() {
		return 2;
	}
}
