package memory.layer;

import structs.FrameData;

/**
 * a layer will convert a frame data to a coded value
 * example: 100 hp -> code 3 in HP layer
 * @author perya
 *
 */
abstract public class MemoryLayer {
	abstract public int getCode(FrameData fData, boolean player);

	abstract public int classify(int data);

	abstract public int getRange();
}
