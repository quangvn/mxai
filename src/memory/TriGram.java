package memory;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import enumerate.Action;

public class TriGram {
	int[][][] freq3;

	int[][] freq2; // total count - 2 actions
	int[][] freq2i; // in game coun - 2 actions

	int[] freq2c; // total count - 1 action
	int[] freq2ic; // total in game count - 1 action

	double[][] freq2agg; // aggregated freqs - 2 actions
	double[] freq2sum; // aggregated freq2 - 1 action

	static final Action[] actions = Action.values();
	static final int actionCount = actions.length;
	static final String DATA_FILE = "/home/perya/projects/Fighting/data/aiData/tg_data.txt";
	static final int maxCount = 1000;
	static final Random rand = new Random();

	private void rescale(int a) {
		for (int i=0; i<actionCount; i++) {
			freq2[a][i] /= 10;
		}
	}

	public void store() {
		try {
			PrintWriter pw = new PrintWriter(new BufferedWriter (new FileWriter(DATA_FILE, false)));
			// print 3gram
			/*
			for (int i=0; i<actionCount; i++) {
				for (int j=0; j<actionCount; j++) {
					for (int k=0; k<actionCount; k++) {
						pw.println(freq3[i][j][k]);
					}
				}
			}
			*/

			// update ingame stats
			for (int i=0; i<actionCount; i++) {
				freq2c[i] += freq2ic[i];
				for (int j=0; j<actionCount; j++) {
					freq2[i][j] += freq2i[i][j];
				}
				if (freq2c[i] > maxCount) rescale(i);
			}

			// print 2gram
			for (int i=0; i<actionCount; i++) {
				for (int j=0; j<actionCount; j++) {
					pw.println(freq2[i][j]);
				}
			}
			pw.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	public void load() {
		freq3 = new int[actionCount][actionCount][actionCount];
		freq2 = new int[actionCount][actionCount];
		freq2c = new int[actionCount];
		freq2i = new int[actionCount][actionCount];
		freq2ic = new int[actionCount];
		freq2agg = new double[actionCount][actionCount];
		freq2sum = new double[actionCount];

		File file = new File(this.DATA_FILE);
		BufferedReader br;

		try {
			br = new BufferedReader(new FileReader(file));
			// 3gram
			/*
			for (int i=0; i<actionCount; i++) {
				for (int j=0; j<actionCount; j++) {
					for (int k=0; k<actionCount; k++) {
						freq3[i][j][k] = Integer.valueOf(br.readLine());
					}
				}
			}
			*/
			// 2gram
			for (int i=0; i<actionCount; i++) {
				for (int j=0; j<actionCount; j++) {
					freq2[i][j] = Integer.valueOf(br.readLine());
					freq2c[i] += freq2[i][j];
				}
			}

			br.close();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}

		for (int i=0; i<actionCount; i++) {
			for (int j=0; j<actionCount; j++) {
				freq2agg[i][j] = freq2[i][j]/(freq2c[i]+1);
				freq2sum[i] += freq2agg[i][j];
			}
		}
	}

	private void learn(List<int[]> data) {
		int len = data.size();
		int s = data.get(0).length -1;
		for (int i=2; i<len; i++) {
			if (data.get(i-2)[s] == 1 || data.get(i-1)[s] == 1 || data.get(i)[s] == 1) continue;
			this.freq3[data.get(i-2)[s]-1][data.get(i-1)[s]-1][data.get(i)[s]-1] ++;
			this.freq2[data.get(i-1)[s]-1][data.get(i)[s]-1] ++;
		}
	}

	public void update(int a1, int a2) {
		this.freq2i[a1][a2]++;
		this.freq2ic[a1] ++;

		double coef = support.Mathf.sigmoid(freq2ic[a1]-10);
		freq2sum[a1] -= freq2agg[a1][a2];
		freq2agg[a1][a2] = Math.pow((1.0-coef)*freq2[a1][a2]/freq2c[a1] + coef*freq2i[a1][a2]/(freq2ic[a1]+1), 2);
		freq2sum[a1] += freq2agg[a1][a2];
	}

	public List<Action> getFreqActions(int a1, int a2, int noActs) {
		List<Action> res = new ArrayList<Action>();
		boolean[] mark = new boolean[actionCount];
		for (int i=0; i<noActs; i++) {
			int highestFreq = 0;
			int indexA = 1;
			for (int j=0; j<actionCount; j++) {
				if (!mark[j])
					if (freq3[a1][a2][j] > highestFreq) {
						highestFreq = freq3[a1][a2][j];
						indexA = j;
					}
			}
			res.add(actions[indexA]);
			mark[indexA] = true;
		}
		return res;
	}

	private int softmax(double[] a, double sum) {
		double rd = rand.nextDouble()*sum;
		int res = 0;
		while (rd>0) {
			rd -= a[res];
			res++;
		}
		if (res == 0) {
			return rand.nextInt(a.length);
		}
		return res-1;
	}

	private int softmax(int[] a, int sum) {
		int rd = rand.nextInt(sum);
		int res = 0;
		while (rd>0) {
			rd -= a[res];
			res++;
		}
		if (res == 0) {
			return rand.nextInt(a.length);
		}
		return res-1;
	}

	public List<Action> getFreqActions(int a, int noActs) {
		List<Action> res = new ArrayList<Action>();

		for (int i=0; i<noActs; i++) {
			res.add(actions[softmax(freq2agg[a], freq2sum[a])]);
		}

		return res;
	}

	public List<Action> getOldFreqActions(int a, int noActs) {
		List<Action> res = new ArrayList<Action>();

		for (int i=0; i<noActs; i++) {
			res.add(actions[softmax(freq2[a], freq2c[a]+1)]);
		}

		return res;
	}

	// generate data file
	public static void main(String[] args) {
		File file = new File("/home/perya/projects/Fighting/data/aiData/data_train.csv");
		TriGram tg = new TriGram();
		tg.freq2 = new int[actionCount][actionCount];
		tg.freq3 = new int[actionCount][actionCount][actionCount];

		Reader in;
		try {
			in = new FileReader(file);
			Iterable<CSVRecord> records = CSVFormat.EXCEL.parse(in);
			List<int[]> data = new ArrayList<int[]>();
			for (CSVRecord record : records) {
				int[] fData = new int[record.size()];
		    for (int i=0; i<record.size(); i++) {
		    	fData[i] = Integer.parseInt(record.get(i));
		    	data.add(fData);
		    }
			}
			tg.learn(data);

			tg.store();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
}