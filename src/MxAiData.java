

import enumerate.Action;
import enumerate.State;
import gameInterface.AIInterface;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.LinkedList;
import java.util.Vector;

import simulator.Simulator;
import structs.CharacterData;
import structs.FrameData;
import structs.GameData;
import structs.Key;
import structs.MotionData;
import support.DataExtractor;
import support.NeuralNetwork;
import commandcenter.CommandCenter;

/**
 * Minimax-liked tree search
 *
 * @author Quang Vu
 */
public class MxAiData implements AIInterface {

  private Simulator simulator;
  private Key key;
  private CommandCenter commandCenter;
  private boolean playerNumber;
  private GameData gameData;

  /** Main FrameData */
  private FrameData frameData;

  /** Data with FRAME_AHEAD frames ahead of FrameData */
  private FrameData simulatorAheadFrameData;

  /** Number of adjusted frames (following the same recipe in JerryMizunoAI) */
  private static final int FRAME_AHEAD = 14;

  private MxNode rootNode;

  /** True if in debug mode, which will output related log */
  public static final boolean DEBUG_MODE = false;
  
  /** True if output log match result to file */
  public static final boolean LOG_MATCH_MODE = true;

  /** log data file */
  private static final String LOG_FILE_NAME = "data/aiData/log_";
  
  // neural network
  NeuralNetwork nn;
  
  // For extracting data
  private DataExtractor de;
  Thread thread;
	
  
  @Override
  public void close() {
		// wait for data extractor to finish
		de.stop();
		try {
			thread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  }

  @Override
  public String getCharacter() {
    return CHARACTER_ZEN;
  }

  @Override
  public void getInformation(FrameData frameData) {
    this.frameData = frameData;
    this.commandCenter.setFrameData(this.frameData, playerNumber);

    // output the frameData
		if (frameData.getP1() != null && frameData.getP2() != null) {
			if (canProcessing() && !commandCenter.getskillFlag())
				de.printFrameData(frameData, frameData.getP1(), frameData.getP2());
		}
  }

  @Override
  public int initialize(GameData gameData, boolean playerNumber) {
    this.playerNumber = playerNumber;
    this.gameData = gameData;

    this.key = new Key();
    this.frameData = new FrameData();
    this.commandCenter = new CommandCenter();

    simulator = gameData.getSimulator();
    
		// loading weights
		nn = new NeuralNetwork();
		nn.load("data/aiData/weights.txt");
		
		
		MxNode.myMotion = this.playerNumber ? gameData.getPlayerOneMotion() : gameData.getPlayerTwoMotion();
    MxNode.oppMotion = this.playerNumber ? gameData.getPlayerTwoMotion() : gameData.getPlayerOneMotion();
    
    
    // new log thread
 		Date d = new Date();
 		String filename = "data/aiData/log_" + 
 						this.gameData.getPlayerOneCharacterName() + "_" + this.gameData.getPlayerTwoCharacterName() + ".csv";
 		de = new DataExtractor(filename, this.playerNumber, this.gameData.getStageXMax(), this.gameData.getStageYMax());
 		
 		// set player and character name for log
 		de.myCharName = this.gameData.getMyName(this.playerNumber);
 		de.oppCharName = this.gameData.getOpponentName(this.playerNumber);
 		
 		thread = new Thread(de);
 		System.out.println("Starting data extractor thread...");
 		thread.start();

 		return 0;
  }

  @Override
  public Key input() {
    return key;
  }

  @Override
  public void processing() {

    if (canProcessing()) {
      if (commandCenter.getskillFlag()) {
        key = commandCenter.getSkillKey();
      } else {
        key.empty();
        commandCenter.skillCancel();

        long st = System.currentTimeMillis();

        prepare(); // Some preparation for MCTS
        rootNode =
            new MxNode(simulatorAheadFrameData, null, gameData, playerNumber,
                commandCenter, nn);

        Action bestAction = rootNode.getBestAction(); // Perform search
        
        if (MxAiData.DEBUG_MODE) {
//          rootNode.printNode(rootNode);
        	// System.out.println(bestAction.name());
        	System.out.println(System.currentTimeMillis() - st);
        }

        commandCenter.commandCall(bestAction.name()); // Perform an action selected by MCTS
      }
    }
  }

  /**
   * Determine whether or not the AI can perform an action
   *
   * @return whether or not the AI can perform an action
   */
  public boolean canProcessing() {
    return !frameData.getEmptyFlag() && frameData.getRemainingTime() > 0;
  }

  /**
   * Some preparation
   * Perform the process for obtaining FrameData with 14 frames ahead
   */
  public void prepare() {
    simulatorAheadFrameData = simulator.simulate(frameData, playerNumber, null, null, FRAME_AHEAD);
  }
}