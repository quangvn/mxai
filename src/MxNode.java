

import java.util.Date;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Random;
import java.util.Vector;

import simulator.Simulator;
import structs.CharacterData;
import structs.FrameData;
import structs.GameData;
import structs.MotionData;
import support.DataExtractor;
import support.NeuralNetwork;
import commandcenter.CommandCenter;

import enumerate.Action;
import enumerate.State;

/**
 * Node in Minimax
 *
 * @author Quang Vu
 */
public class MxNode {

  /** Depth of tree search */
  public static final int TREE_DEPTH = 3;
  
  /** Number of actions to consider */
  public static final int noActions = 3;

  /** Time for performing simulation */
  public static final int SIMULATION_TIME = 15;

  /** Parent node */
  private MxNode parent;
  
  /** My char action lead to this state from parent */
  private Action selectedMyAction;
  
  /** Opp char action lead to this state from parent */
  private Action selectedOppAction;
  
  /** Child node */
  private MxNode[] children;
  
  /** Node depth */
  private int depth;
  
  /** Evaluation value */
  private double score;

  /** All good actions of self AI */
  private LinkedList<Action> myActions;

  /** Use in simulation */
  private Simulator simulator;

  private FrameData frameData;
  private boolean playerNumber;
  private CommandCenter commandCenter;
  private GameData gameData;

  private Deque<Action> mAction;
  private Deque<Action> oppAction;
  
  private NeuralNetwork nn;
  
  /* static fields */
  private static Action[] actions = Action.values();
  
  private static Action[] actionAir =
      new Action[] {Action.AIR_GUARD, Action.AIR_A, Action.AIR_B, Action.AIR_DA, Action.AIR_DB,
          Action.AIR_FA, Action.AIR_FB, Action.AIR_UA, Action.AIR_UB, Action.AIR_D_DF_FA,
          Action.AIR_D_DF_FB, Action.AIR_F_D_DFA, Action.AIR_F_D_DFB, Action.AIR_D_DB_BA,
          Action.AIR_D_DB_BB};
  private static Action[] actionGround =
      new Action[] {Action.STAND_D_DB_BA, Action.BACK_STEP, Action.FORWARD_WALK, Action.DASH,
          Action.JUMP, Action.FOR_JUMP, Action.BACK_JUMP, Action.STAND_GUARD,
          Action.CROUCH_GUARD, Action.THROW_A, Action.THROW_B, Action.STAND_A, Action.STAND_B,
          Action.CROUCH_A, Action.CROUCH_B, Action.STAND_FA, Action.STAND_FB, Action.CROUCH_FA,
          Action.CROUCH_FB, Action.STAND_D_DF_FA, Action.STAND_D_DF_FB, Action.STAND_F_D_DFA,
          Action.STAND_F_D_DFB, Action.STAND_D_DB_BB};
  private static Action spSkill = Action.STAND_D_DF_FC;
  
  public static Vector<MotionData> myMotion;
  public static Vector<MotionData> oppMotion;
  
  private static Random rand = new Random();
  
  private boolean isCreateNode;


  public MxNode(FrameData frameData, MxNode parent, GameData gameData, boolean playerNumber,
      CommandCenter commandCenter, NeuralNetwork nnet, Action selectedMyAction, Action selectedOppAction) {
    
  	this(frameData, parent, gameData, playerNumber, commandCenter, nnet);
    this.selectedMyAction = selectedMyAction;
    this.selectedOppAction = selectedOppAction;
  }

  public MxNode(FrameData frameData, MxNode parent, GameData gameData, boolean playerNumber,
      CommandCenter commandCenter, NeuralNetwork nnet) {
    this.frameData = frameData;
    this.parent = parent;
    this.gameData = gameData;
    this.simulator = new Simulator(gameData);
    this.playerNumber = playerNumber;
    this.commandCenter = commandCenter;
    this.nn = nnet;

    this.selectedMyAction = null;
    
    this.mAction = new LinkedList<Action>();
    
    // set the base score, will be update if expanded
    this.score = this.frameData.getMyCharacter(this.playerNumber).getHp() 
    								- this.frameData.getOpponentCharacter(this.playerNumber).getHp();

    if (this.parent != null) {
      this.depth = this.parent.depth + 1;
    } else {
      this.depth = 0;
    }
  }
  
  /**
   * return the index of maximum from an array
   */
  private int getMax(double[] arr) {
  	double m = -99999;
  	int res = -1;
  	for (int i=0; i<arr.length; i++) {
  		if (arr[i] > m) {
  			m = arr[i];
  			res = i;
  		}
  	}
  	
  	return res;
  }
  
  /**
   * Get 3 best actions
   * @param arr
   * @param noB
   * @param myActions
   */
  private void getBest(double[] arr, int noB, LinkedList<Action> myActions) {
  	boolean[] m = new boolean[arr.length];
  	int[] b = new int[noB];
  	double _max;
  	
  	for (int i=0; i<m.length; i++) m[i] = false;
  	for (int i=0; i<noB; i++) {
  		_max = -9999;
  		for (int j=0; j<arr.length; j++) 
  			if (!m[j]) {
  				if (arr[j] > _max) {
  					_max = arr[j];
  					b[i] = j;
  				}
  			}
  		m[b[i]] = true;
  	}
  	
  	for (int i=0; i<noB; i++) {
  		myActions.add(actions[b[i]]);
  	}
  }
  
  public Action getRandomAction(CharacterData myCharacter) {
    if (myCharacter.getState() == State.AIR) {
    	return actionAir[rand.nextInt(actionAir.length)];
    } else {
    	// special skill have priority
//    	if (Math.abs(myMotion.elementAt(Action.valueOf(spSkill.name()).ordinal())
//          .getAttackStartAddEnergy()) <= myCharacter.energy) {
//        return spSkill;
//      }

    	return actionGround[rand.nextInt(actionGround.length)];
    }
  }


  /**
   * Generate a node
   */
  public void createNode() {
  	double[] input;
  	double[] output;
  	
  	// get 3 best possible moves
  	LinkedList<Action> myActions = new LinkedList<Action>();
  	input = DataExtractor.extractFeatures(frameData, frameData.getOpponentCharacter(playerNumber),
  																									 frameData.getMyCharacter(playerNumber));
  	output = nn.calculate(input);
  	getBest(output, noActions, myActions);
  	// plus a random action
  	myActions.add(getRandomAction(frameData.getMyCharacter(playerNumber)));
  	// myActions.add(getRandomAction(frameData.getMyCharacter(playerNumber)));
  	
  	
  	// get best possible moves for opp
  	oppAction = new LinkedList<Action>();
  	input = DataExtractor.extractFeatures(frameData, frameData.getMyCharacter(playerNumber),
  																									 frameData.getOpponentCharacter(playerNumber));	
  	output = nn.calculate(input);
  	Action opp = actions[getMax(output)];
  	oppAction.add(opp);
  	
    this.children = new MxNode[myActions.size()]; 

    for (int i = 0; i < children.length; i++) {
    	mAction.clear();
    	mAction.add(myActions.get(i));
    	FrameData simFrameData = simulator.simulate(frameData, playerNumber, mAction, oppAction, SIMULATION_TIME);
      children[i] =
          new MxNode(simFrameData, this, gameData, playerNumber, commandCenter, nn,
              myActions.get(i), opp);
    }
    
    this.isCreateNode = true;
  }
  
  /**
   * expand the search tree
   */
  public void expand() {
  	if (this.depth < TREE_DEPTH) {
  		if (!this.isCreateNode) {
  			this.createNode();
  		}
  		
  		for (MxNode child : this.children) {
  			child.expand();
  			if (child.score > this.score) {
  				this.score = child.score;
  			}
  		}
  	}
  }
  
  /**
   * choose an action by a Minimax-like procedure
   * @return
   */
  public Action getBestAction() {
  	this.expand();
  	
  	Action res = null;
  	double best_score = -9999;
  	for (MxNode child : this.children) {
  		if (child.score > best_score) {
  			best_score = child.score;
  			res = child.selectedMyAction;
  		}
  	}
  	
  	return res;
  }

  public void printNode(MxNode node) {
    for (int i = 0; i < node.children.length; i++) {
    }
    System.out.println("");
    for (int i = 0; i < node.children.length; i++) {
      if (node.children[i].isCreateNode) {
        printNode(node.children[i]);
      }
    }
  }
}