import java.util.Date;
import java.util.Random;

import commandcenter.CommandCenter;
import enumerate.Action;
import gameInterface.AIInterface;
import simulator.Simulator;
import structs.CharacterData;
import structs.FrameData;
import structs.GameData;
import structs.Key;
import support.DataExtractor;
import support.NeuralNetwork;

public class RL implements AIInterface {

	private Key inputKey;
	private boolean player;
	private FrameData frameData;
	private CommandCenter cc;
	private Simulator simulator;
	private GameData gd;

	private CharacterData opp;
	private CharacterData my;
	private boolean isGameJustStarted;

	private Action[] actions;

	private Random rand = new Random();

	NeuralNetwork nn;

	private DataExtractor de;
	Thread thread;

	@Override
	public void close() {
		// wait for data extractor to finish
		de.stop();
		try {
			thread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public String getCharacter() {
		// Select the player ZEN as per competition rules
		return CHARACTER_ZEN;
	}

	@Override
	public void getInformation(FrameData frameData) {
		// Load the frame data every time getInformation gets called
		this.frameData = frameData;

    // output the frameData
		if (frameData.getP1() != null && frameData.getP2() != null) {
			if (canProcessing() && !cc.getskillFlag())
				de.printFrameData(frameData, frameData.getOpponentCharacter(player), frameData.getMyCharacter(player));
		}
	}

	@Override
	public int initialize(GameData arg0, boolean player) {
		// Initialize the global variables at the start of the round
		inputKey = new Key();
		this.player = player;
		frameData = new FrameData();
		cc = new CommandCenter();
		gd = arg0;
		simulator = gd.getSimulator();
		isGameJustStarted = true;

		// actions
		this.actions = Action.values();

		// loading weights
		nn = new NeuralNetwork();
		nn.load("data/aiData/weights.txt");

		// new log thread
		Date d = new Date();
		String filename = "data/aiData/log_" + gd.getPlayerOneCharacterName() + "_" + gd.getPlayerTwoCharacterName() + ".csv";
		de = new DataExtractor(filename, player, gd.getStageXMax(), gd.getStageYMax());
		thread = new Thread(de);
		System.out.println("Starting data extractor thread...");
		thread.start();
		return 0;
	}

	private int selectBest(double[] arr) {
		if (rand.nextInt(6) == 2) {
			return rand.nextInt(56);
		}
		double max = -9999;
		int res = -1;
		for (int i=0; i<arr.length; i++) {
			if (max < arr[i]) {
				max = arr[i];
				res = i;
			}
		}
		return res;
	}

	@Override
	public Key input() {
		return inputKey;
	}

	@Override
	public void processing() {
		if (canProcessing()) {
			if (cc.getskillFlag()) {
		        inputKey = cc.getSkillKey();
		    }
			else {
				// frameData = simulator.simulate(frameData, this.player, null, null, 14);

				cc.setFrameData(frameData, player);

				my = cc.getMyCharacter();
				opp = cc.getEnemyCharacter();

				// We empty the keys and cancel skill just in case
				inputKey.empty();
				cc.skillCancel();

				double[] input = DataExtractor.extractFeatures(frameData, frameData.getOpponentCharacter(player), frameData.getMyCharacter(player));

				double[] output = nn.calculate(input);
				if (output.length != 56) {
					System.out.println("WWWWWWW output length is not correct" + output.length);
				}
				int action = selectBest(output);

				cc.commandCall(this.actions[action].name());
			}
		}
		else isGameJustStarted = true;
	}

    /**
     * Determine whether or not the AI can perform an action
     *
     * @return whether or not the AI can perform an action
     */
    public boolean canProcessing() {
        return !frameData.getEmptyFlag() && frameData.getRemainingTime() > 0;
    }
}
