package support;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import fighting.Attack;
import structs.CharacterData;
import structs.FrameData;

public class DataExtractor implements Runnable {

	private static final char SEP = ',';

	private ConcurrentLinkedQueue<FrameData> vF;
	private Queue<CharacterData> vP1, vP2;
	private File file;
	private int lastRound;
	private int oppScore;
	private int myScore;


	private static boolean player;
	private static int stageX;
	private static int stageY;

	// for log

	 /** log match results file */
  public String LOG_MATCH_RESULT_FILE_NAME = "data/aiData/log_match_res.csv";

	public String myCharName;
	public String oppCharName;

	public String myPlayerName;
	public String oppPlayerName;


	private boolean running;

	public DataExtractor(String filename, boolean player, int stageX, int stageY) {
		this.file = new File(filename);
		DataExtractor.player = player;
		DataExtractor.stageX = stageX;
		DataExtractor.stageY = stageY;
		vF = new ConcurrentLinkedQueue<FrameData>();
		vP1 = new ConcurrentLinkedQueue<CharacterData>();
		vP2 = new ConcurrentLinkedQueue<CharacterData>();

		lastRound = 0;
		myScore = 0;
		oppScore = 0;
	}

	public void stop() {
		printResult();
		running = false;
	}


	public void printFrameData(FrameData fData, CharacterData p1, CharacterData p2) {
		vF.add(fData);
		vP1.add(p1);
		vP2.add(p2);
	}

	public boolean isPrinting() {
		return !vF.isEmpty();
	}

	private static int easyPrint(boolean b) {
		return (b ? 1 : 0);
	}

	/**
	 * return a list of data about frame and players
	 * @param fData
	 * @param p1
	 * @param p2
	 * @return
	 */
	public static double[] extractFeatures(FrameData fData, CharacterData p1, CharacterData p2) {
		List<Integer> res = new ArrayList<Integer>(36);

		// res.add(easyPrint(player));
		// res.add(stageX); res.add(stageY);

		// opponent
		res.addAll(extractPlayerFeatures(p1, fData));
		res.addAll(extractPlayerAttack(p1.attack, fData));
		res.add(p1.getAction().ordinal());

		// ours
		res.addAll(extractPlayerFeatures(p2, fData));
		res.addAll(extractPlayerAttack(p2.attack, fData));
		res.add(p2.getAction().ordinal());

		double[] ans = new double[res.size()];

		for (int i=0; i<ans.length; i++) {
			ans[i] = res.get(i);
		}

		return ans;
	}

	/**
	 * return a list of data about player current attack
	 * 20
	 */
	private static List<Integer> extractPlayerAttack(Attack att, FrameData fData) {
		List<Integer> res = new ArrayList<Integer>();

		// current frame
		// 3
		res.add(att.getNowFrame());
		res.add(att.getStartUp());
		// col 20: distance to opponent
		res.add(Math.abs(fData.getP1().left - fData.getP2().left));
		res.add(att.getActive());

		// hit area
		// 8
		res.add(att.getHitAreaSetting().getL());
		res.add(att.getHitAreaSetting().getR());
		res.add(att.getHitAreaSetting().getB());
		res.add(att.getHitAreaSetting().getT());

		res.add(att.getHitAreaNow().getL());
		res.add(att.getHitAreaNow().getR());
		res.add(att.getHitAreaNow().getB());
		res.add(att.getHitAreaNow().getT());
		
		// col 30: distance to opponent
		res.add(Math.abs(fData.getP1().left - fData.getP2().left));

		// speed
		// 4
		/*
		res.add(att.getSettingSpeedX());
		res.add(att.getSettingSpeedY());
		res.add(att.getSpeedX());
		res.add(att.getSpeedY());
		*/

		// recovery
		// 2
		res.add(att.getGiveGuardRecov());
		res.add(easyPrint(att.isDownProperty()));

		// type
		// 1
		res.add(att.getAttackType());

		// damage
		// 2
		res.add(att.getHitDamage());
		res.add(att.getGuardDamage());

		// energy
		// 4
		res.add(att.getStartAddEnergy());
		res.add(att.getHitAddEnergy());
		res.add(att.getGuardAddEnergy());
		res.add(att.getGiveEnergy());

		return res;
	}


	/**
	 * return a list of data about player
	 * 16
	 * @param cData
	 * @return
	 */
	public static List<Integer> extractPlayerFeatures(CharacterData cData, FrameData fData) {
		List<Integer> res = new ArrayList<Integer>();

		res.add(cData.getComboState());
		res.add(cData.getEnergy());

		// 4
		// character center position
		res.add(cData.getGraphicCenterX());
		res.add(cData.getGraphicCenterY());
		// size of character
		res.add(cData.getGraphicSizeX());
		res.add(cData.getGraphicSizeY());
		res.add(cData.getHp());
		// frame value of last combo
		// res.add(cData.getLastCombo());

		// hit box position
		res.add(cData.getLeft());
		res.add(cData.getRight());
		// col 10: distance to opponent
		res.add(Math.abs(fData.getP1().left - fData.getP2().left));
		res.add(cData.getBottom());
		res.add(cData.getTop());

		// remaining frame of current action
		res.add(cData.getRemainingFrame());
		// top-left of character box
		res.add(cData.getX());
		res.add(cData.getY());
		// character facing direction
		res.add(easyPrint(cData.isFront()));
		// can accept a new action
		res.add(easyPrint(cData.isControl()));

		return res;
	}


	/**
	 * return a list of data about frame and players
	 */
	public static List<Integer> extractFeaturesForPrint(FrameData fData, CharacterData p1, CharacterData p2) {
		List<Integer> res = new ArrayList<Integer>(36);

		// res.add(easyPrint(player));
		// res.add(stageX); res.add(stageY);

		// opponent
		res.addAll(extractPlayerFeatures(p1, fData));
		res.addAll(extractPlayerAttack(p1.attack, fData));
		res.add(p1.getAction().ordinal());

		// ours
		res.addAll(extractPlayerFeatures(p2, fData));
		res.addAll(extractPlayerAttack(p2.attack, fData));
		res.add(p2.getAction().ordinal());

		return res;
	}

	/**
	 *
	 */
	private void extractFrameData() {
		FrameData frameData = vF.poll();
		CharacterData oppChar = vP1.poll();
		CharacterData myChar = vP2.poll();

		// generate output string
		StringBuilder sb = new StringBuilder();

		if (oppChar != null && myChar != null) {
			List<Integer> out = extractFeaturesForPrint(frameData, oppChar, myChar);

			Iterator<Integer> it = out.iterator();
			while (it.hasNext()) {
				sb.append(it.next()).append(SEP);
			}
			sb.deleteCharAt(sb.length()-1);

			try {
				PrintWriter pw = new PrintWriter(new BufferedWriter (new FileWriter(file, true)));
				pw.println(sb.toString());
				pw.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


			if (frameData.getRound() != this.lastRound) {
				printResult();
				this.lastRound = frameData.getRound();
			}
			this.myScore = myChar.getHp();
			this.oppScore = oppChar.getHp();
		}
	}

	private void printResult() {
		try {
			PrintWriter pw = new PrintWriter(new BufferedWriter (new FileWriter(LOG_MATCH_RESULT_FILE_NAME, true)));
			StringBuilder sb = new StringBuilder();
			// sb.append(this.oppPlayerName).append(SEP);
			// sb.append(this.myPlayerName).append(SEP);
			sb.append(this.oppCharName).append(SEP);
			sb.append(this.myCharName).append(SEP);
			sb.append(this.oppScore).append(SEP);
			sb.append(this.myScore).append(SEP);
			sb.append((-this.myScore*1000)/(-this.oppScore-this.myScore+1)).append(SEP);
			sb.append((-this.oppScore*1000)/(-this.oppScore-this.myScore+1)).append(SEP);

			pw.println(sb.toString());
			pw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		running = true;
		System.out.println("Data Extractor thread started");
		while (running) {
			if (!vF.isEmpty()) {
				extractFrameData();
			}
		}
		System.out.println("Data Extractor thread stopped");
	}
}
