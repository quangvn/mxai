package support;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import Jama.Matrix;

public class NeuralNetwork {
	// fields
	private int input_size;
	private int output_size;
	// number of cnn layers
	private int no_cnn;
	// size of input matrix
	private int isize_x;
	private int isize_y;

	 // number of filters in each cnn layers
	private int[] cnn_layer_filters;
	// size of each layer-filter
	private int[][] cnn_lfx = new int[10][10];
	private int[][] cnn_lfy = new int[10][10];
	private int[][] cnn_lfsize = new int[10][10];
	private int[] cnn_output_length;

	// number of fully connected layers
	private int no_fnn;

	// list of matrixes of weights of cnn filters in layers
	private List<List<Matrix>> cnn_filters;

	// list of matrixes of weights of fnn layers
	private List<Matrix> fnn_layers;
	private int input_fnn_length; // output of cnn layer


	public NeuralNetwork() {}

	/*
	 * currently, the input matrix size is fixed at 5x7
	 * @param filename
	 * @return
	 */
	public boolean load(String filename) {
		File file = new File(filename);
		BufferedReader br;

		try {
			br = new BufferedReader(new FileReader(file));

			this.input_size = Integer.valueOf(br.readLine());
			this.output_size = Integer.valueOf(br.readLine());

			// number of cnn layers, currently fixed at 1
			this.no_cnn = Integer.valueOf(br.readLine());
			this.no_fnn = Integer.valueOf(br.readLine());

			// number of filter in each cnn layer
			if (this.no_cnn > 0) {
				// input matrix size, currently fixed at 7x5
				this.isize_x = Integer.valueOf(br.readLine());
				this.isize_y = Integer.valueOf(br.readLine());

				this.cnn_layer_filters = new int[this.no_cnn];
				this.cnn_filters = new ArrayList<List<Matrix>>();

				// for each layer
				for (int i=0; i<this.no_cnn; i++) {
					this.cnn_layer_filters[i] = Integer.valueOf(br.readLine());
					this.cnn_filters.add(new ArrayList<Matrix>());
					// for each filter
					for (int j=0; j<this.cnn_layer_filters[i]; j++) {
						// read the size of filter
						int x, y;
						this.cnn_lfx[i][j] = Integer.valueOf(br.readLine());
						this.cnn_lfy[i][j] = Integer.valueOf(br.readLine());
						x = this.cnn_lfx[i][j];
						y = this.cnn_lfy[i][j];
						this.cnn_lfsize[i][j] = x*y+1;

						double[][] m = new double[x*y+1][1];
						for (int k=0; k<x*y+1; k++) {
								m[k][0] = Double.valueOf(br.readLine());
						}
						this.cnn_filters.get(i).add(new Matrix(m));
					}
				}
			}


			if (this.no_fnn > 0) {
				this.fnn_layers = new ArrayList<Matrix>();
				for (int i=0; i<this.no_fnn; i++) {
					int x,y;
					x = Integer.valueOf(br.readLine());
					y = Integer.valueOf(br.readLine());

					if (i==1) this.input_fnn_length = x;

					double[][] m = new double[x][y];
					for (int j=0; j<x; j++) {
						for (int k=0; k<y; k++) {
							m[j][k] = Double.valueOf(br.readLine());
						}
					}

					this.fnn_layers.add(new Matrix(m));
				}
			}

			// prepare cnn layer output length
			this.cnn_output_length = new int[this.no_cnn];
			for (int i=0; i<this.no_cnn; i++) {
				this.cnn_output_length[i] = 0;
				for (int j=0; j<this.cnn_layer_filters[i]; j++) {
					this.cnn_output_length[i] += (this.isize_x - this.cnn_lfx[i][j] + 1) * (this.isize_y - this.cnn_lfy[i][j] + 1);
				}
			}

			br.close();
		} catch (IOException e) {
			this.load(filename);
			// e.printStackTrace();
		}
		return true;
	}

	private double tanh(double input) {
		return Math.tanh(input);
	}

	private double ReLU(double input) {
		return input > 0 ? input : 0;
	}

	/*
	 * flatten a matrix to a vector (a 1 x m matrix)
	 */
	public double[][] flatten(double[][] m) {
		double[][] ans = new double[1][];
		for (int i=0; i<m.length; i++){
			for (int j=0; j<m[i].length; j++) {
				ans[0][i*m[i].length+j] = m[i][j];
			}
		}
		return ans;
	}

	public int apply_cnn_filter(Matrix grid, int iLayer, int iFilter, Matrix output, int out_pos) {
		int grid_row = grid.getRowDimension();
		int grid_col = grid.getColumnDimension();
		int filt_row = this.cnn_lfx[iLayer][iFilter];
		int filt_col = this.cnn_lfy[iLayer][iFilter];
		int filt_size = this.cnn_lfsize[iLayer][iFilter];

		int x, y, p;
		Matrix filter;
		double temp;
		for (int i=0; i<grid_row - filt_row + 1; i++)
			for (int j=0; j<grid_col - filt_col + 1; j++) {
				filter = this.cnn_filters.get(iLayer).get(iFilter);
				x = i;
				y = j;
				p = 0;
				temp = 0;
				while (p<filt_size-1) {
					temp += filter.get(p,0) * grid.get(x,y);
					p ++;
					y ++;
					if (y == j + filt_col) {
						y = j;
						x ++;
					}
				}
				temp += filter.get(p,0);
				output.set(out_pos, 0, ReLU(temp));
				out_pos++;
			}
		return out_pos;
	}

	public double[] calculate(double[] input) {
		Matrix input_fnn;

		if (this.no_cnn == 0) {
			input_fnn = new Matrix(input, input.length);
		} else {
			Matrix grid;
			Matrix output = new Matrix(input, input.length);
			for (int i=0; i<this.no_cnn; i++) {
				// prepare input matrix, transform a 'output' (1d) to 'grid' (2d)
				grid = new Matrix(output.getColumnPackedCopy(), this.isize_x);
//				System.out.println("Grid size: " + grid.getRowDimension() + ", " + grid.getColumnDimension());
				// calculate length of output
				output = new Matrix(this.cnn_output_length[i], 1);
				int out_pos = 0;

				// for each filter
				for (int j=0; j<this.cnn_layer_filters[i]; j++) {
					out_pos = apply_cnn_filter(grid, i, j, output, out_pos);
				}
			}
			input_fnn = output;
		}
// 		System.out.println("Input to fnn dimension: " + input_fnn.getRowDimension() + ", " + input_fnn.getColumnDimension());
		// fnn
		for (int i=0; i<this.no_fnn; i++) {

			Matrix t = new Matrix(input_fnn.getRowDimension()+1, 1);
			for (int j=0; j<t.getRowDimension()-1; j++) {
				t.set(j, 0, input_fnn.get(j, 0));
			}
			t.set(t.getRowDimension()-1, 0, 1);

			input_fnn = this.fnn_layers.get(i).times(t);
			if (i< this.no_fnn-1) {
				for (int j=0; j<input_fnn.getRowDimension(); j++) {
					input_fnn.set(j, 0, tanh(input_fnn.get(j, 0)));
				}
			}
		}


//		System.out.println(input_fnn.getColumnDimension());
//		System.out.println(input_fnn.getRowDimension());

		double[] t = new double[input_fnn.getRowDimension()];
		for (int ii=0; ii<input_fnn.getRowDimension(); ii++) {
			t[ii] = input_fnn.get(ii, 0);
		}

		return t;
	}
}
