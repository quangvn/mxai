package search;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Random;

import enumerate.Action;
import memory.HashMemory;
import memory.TriGram;
import simulator.Simulator;
import structs.CharacterData;
import structs.FrameData;
import structs.GameData;

class MPNode {
	int visit;
	double score;
	double aggScore;
	double ucb;

	List<MPNode> children;

	MPNode parent;
	int depth;

	Action selectedAction;

	MPNode() {
		parent = null;
		depth = 0;
		this.score = 0;
		this.visit = 1;
	}

	MPNode(MPNode parent, Action selectedAction) {
		this.parent = parent;
		this.selectedAction = selectedAction;
		this.depth = parent.depth + 1;
		this.visit = 1;
		this.score = 0;
		this.setUCB();
	}

	void expand(int noActs) {
		List<Action> actions = MPTree.tg.getFreqActions(this.selectedAction.ordinal(), noActs);

		assert(actions.size() > 0);

		this.children = new ArrayList<MPNode>();
		for (Action a : actions) {
			this.children.add(new MPNode(this, a));
		}
	}

	void expand(List<Action> actions) {
		this.children = new ArrayList<MPNode>();
		for (Action a : actions) {
			this.children.add(new MPNode(this, a));
		}
	}

	void expand() {
		this.expand(8);
	}

	double getUCB() {
		return this.score/this.visit + MPTree.UCB_C * Math.sqrt((2 * Math.log(parent.visit)) / this.visit);
	}

	void setUCB() {
		 if (this.visit == 0) {
       this.ucb = 9999 + MPTree.rand.nextInt(50);
     } else {
       this.ucb = this.getUCB();
     }
	}

	MPNode selectChildUCB() {
		MPNode selectedNode = null;
    double bestUcb = -9999;

    for (MPNode child : this.children) {
    	child.setUCB();
      if (bestUcb < child.ucb) {
        selectedNode = child;
        bestUcb = child.ucb;
      }
    }

    assert(selectedNode != null);
    return selectedNode;
	}

	/*
	 * return current node if is leaf and depth = max depth
	 * otherwise expand and return a child
	 */
	MPNode getLeafUCB() {
		if (this.depth == MPTree.UCT_TREE_DEPTH || this.visit < 10) {
			return this;
		}

		if (this.children == null) {
			this.expand();
		}

		return this.selectChildUCB().getLeafUCB();
	}

	Deque<Action> traceActions() {
		ArrayDeque<Action> actions = new ArrayDeque<Action>();
		MPNode curNode = this;
		while (curNode.parent != null) {
			actions.add(curNode.selectedAction);
			curNode = curNode.parent;
		}
		return actions;
	}

	void update(double score) {
		this.score += score;
		this.visit ++;

		if (this.parent != null) {
			// this.setUCB();
			this.parent.update(score);
		}
	}
}

public class MPTree {
	MPNode selfRootNode;
	MPNode oppRootNode;

	boolean playerNumber;
	FrameData fData;
	GameData gData;
	Simulator simulator;

	int myOriHP;
	int oppOriHP;

  private List<Action> myPossibleActions;
  private List<Action> oppPossibleActions;

	int playoutCount;

	static int SIMULATION_TIME = 60;
	static int SIMULATION_ACTS = 5;
	static int UCT_TREE_DEPTH = 2;
	static double UCB_C = 3;
	static int UCT_B = 5;
	static int TG_B = 4;
	static int OPP_UCT_B = 10;
	static final Random rand = new Random();

	public static TriGram tg;
	public static HashMemory hm;

	static Action[] actions;

	static long availableTime = 16000000; // time in nano-second

	public MPTree(FrameData frameData, GameData gameData, boolean playerNumber, List<Action> myPossibleActions, List<Action> oppPossibleActions) {
		this.fData = frameData;
		this.gData = gameData;
		this.simulator = new Simulator(gameData);
		this.playerNumber = playerNumber;
		this.playoutCount = 0;

		this.myOriHP = frameData.getMyCharacter(playerNumber).hp;
		this.oppOriHP = frameData.getOpponentCharacter(playerNumber).hp;

		this.myPossibleActions = myPossibleActions;
		this.oppPossibleActions = oppPossibleActions;

		selfRootNode = new MPNode();
		List<Action> myActions = hm.getBestActions(frameData, playerNumber, UCT_B);
		selfRootNode.expand(myActions);


		oppRootNode = new MPNode();
		List<Action> oppActions = hm.getBestActions2(frameData, !playerNumber, OPP_UCT_B-TG_B);
    if (frameData.getOpponentCharacter(playerNumber) != null) {
    	oppRootNode.selectedAction = frameData.getOpponentCharacter(playerNumber).action;
    } else {
    	oppRootNode.selectedAction = actions[0];
		}
		oppActions.addAll(tg.getFreqActions(oppRootNode.selectedAction.ordinal(), TG_B));
		oppRootNode.expand(oppActions);
	}

	private void uct() {
		MPNode selfLeafNode = selfRootNode.getLeafUCB();
		Deque<Action> selfActions = selfLeafNode.traceActions();

		MPNode oppLeafNode = oppRootNode.getLeafUCB();
		Deque<Action> oppActions = oppLeafNode.traceActions();

		double score = playout(fData, selfActions, oppActions, SIMULATION_TIME);

		selfLeafNode.update(score);
		oppLeafNode.update(-score);
	}

	private double playout(FrameData fData, Deque<Action> selfActions, Deque<Action> oppActions, int SIMULATION_TIME) {
		while (selfActions.size() < SIMULATION_ACTS) {
			selfActions.add(myPossibleActions.get(rand.nextInt(myPossibleActions.size())));
		}

		// Action prevAct = oppActions.peekLast();
		while (oppActions.size() < SIMULATION_ACTS) {
			oppActions.add(oppPossibleActions.get(rand.nextInt(oppPossibleActions.size())));
			// prevAct = tg.getFreqActions(prevAct.ordinal(), 1).get(0);
		  // oppActions.add(prevAct);
		}

		FrameData simulatedFrame = simulator.simulate(fData, playerNumber, selfActions, oppActions, SIMULATION_TIME);
		playoutCount++;

		return getScore(simulatedFrame);
	}

	// basic hp diff
	private double getScore(FrameData fData) {
		CharacterData myCharacter = fData.getMyCharacter(this.playerNumber);
		CharacterData oppCharacter = fData.getOpponentCharacter(this.playerNumber);

		int hpDiff = myCharacter.hp - oppCharacter.hp - (this.myOriHP - this.oppOriHP);
		double eDiff = myCharacter.energy - oppCharacter.energy;
		return hpDiff + eDiff/10; // + 0.5*hm.getBestActionScore(fData, playerNumber);
	}

	public Action getBestAction() {
		long sTime = System.nanoTime();
		while (System.nanoTime() - sTime <= availableTime) {
			uct();
		}
		// System.out.println("PLAYOUT: " + this.playoutCount);
		return getBestScoreAction();
	}

	private Action getBestScoreAction() {
		double bestScore = -999999 ;
		Action bestAction = null;
		// System.out.println("==============");
		for (MPNode node : this.selfRootNode.children) {
			node.aggScore = node.score/node.visit;
			// System.out.println(node.aggScore);
			if (node.aggScore > bestScore) {
				bestScore = node.aggScore;
				bestAction = node.selectedAction;
			}
		}

		int[] actIndex = new int[UCT_B];
		double[] actScore = new double[UCT_B];
		for (int i=0; i<UCT_B; i++) {
			actIndex[i] = selfRootNode.children.get(i).selectedAction.ordinal();
			// actScore[i] = 3*(support.Mathf.sigmoid(selfRootNode.children.get(i).aggScore/20)-0.5);
			actScore[i] = selfRootNode.children.get(i).aggScore;
		}
		System.out.print("Action: " + bestAction.name() + " \t");
		if (bestScore > 0) System.out.println("O");
		else System.out.println("X");
		hm.batchUpdate(fData, playerNumber, actIndex, actScore);
		return bestAction;
	}
}
