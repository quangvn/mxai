package search;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Random;

import enumerate.Action;
import simulator.Simulator;
import structs.CharacterData;
import structs.FrameData;
import structs.GameData;

class McNode {
	int visit;
	double score;
	double aggScore;
	double ucb;

	private List<Action> myPossibleActions;

	List<McNode> children;

	McNode parent;
	int depth;

	Action selectedAction;

	McNode() {
		parent = null;
		depth = 0;
		this.visit = 1;
	}

	McNode(McNode parent, Action selectedAction, List<Action> myPossibleActions) {
		this.parent = parent;
		this.selectedAction = selectedAction;
		if (parent != null) {
			this.depth = parent.depth + 1;
			this.setUCB();
		} else {
			this.depth = 0;
		}
		this.visit = 1;
		this.myPossibleActions = myPossibleActions;
	}

	void expand() {
		this.children = new ArrayList<McNode>();
		for (Action a : myPossibleActions) {
			this.children.add(new McNode(this, a, myPossibleActions));
		}
	}

	double getUCB() {
		return 1.0*this.score/this.visit + McTree.UCB_C * Math.sqrt((2 * Math.log(parent.visit)) / this.visit);
	}

	void setUCB() {
		 if (this.visit == 0) {
       this.ucb = 9999 + McTree.rand.nextInt(50);
     } else {
       this.ucb = this.getUCB();
     }
	}

	McNode selectChildUCB() {
		McNode selectedNode = null;
    double bestUcb = -99999;

    for (McNode child : this.children) {
      if (bestUcb < child.ucb) {
        selectedNode = child;
        bestUcb = child.ucb;
      }
    }

    assert(selectedNode != null);
    return selectedNode;
	}

	/*
	 * return current node if is leaf and depth = max depth
	 * otherwise expand and return a child
	 */
	McNode getLeafUCB() {
		if (this.depth == McTree.UCT_TREE_DEPTH || this.visit < 10) {
			return this;
		}

		if (this.children == null) {
			this.expand();
		}

		assert(this.children.size() > 0);
		return this.selectChildUCB().getLeafUCB();
	}

	Deque<Action> traceActions() {
		ArrayDeque<Action> actions = new ArrayDeque<Action>();
		McNode curNode = this;
		while (curNode.parent != null) {
			actions.add(curNode.selectedAction);
			curNode = curNode.parent;
		}
		return actions;
	}

	void update(double score) {
		this.score += score;
		this.visit ++;

		if (this.parent != null) {
			this.setUCB();
			this.parent.update(score);
		}
	}
}

public class McTree {
	McNode selfRootNode;
	McNode oppRootNode;

	boolean playerNumber;
	FrameData fData;
	GameData gData;
	Simulator simulator;

	int myOriHP;
	int oppOriHP;

  private List<Action> myPossibleActions;
  private List<Action> oppPossibleActions;

	int playoutCount;

	static int SIMULATION_TIME = 60;
	static int SIMULATION_ACTS = 5;
	static int UCT_TREE_DEPTH = 2;
	static double UCB_C = 3;
	static final Random rand = new Random();

	static Action[] actions;

	static long availableTime = 16000000; // time in nano-second

	public McTree(FrameData frameData, GameData gameData, boolean playerNumber, List<Action> myPossibleActions, List<Action> oppPossibleActions) {
		this.fData = frameData;
		this.gData = gameData;
		this.simulator = new Simulator(gameData);
		this.playerNumber = playerNumber;
		this.playoutCount = 0;

		this.myOriHP = frameData.getMyCharacter(playerNumber).hp;
		this.oppOriHP = frameData.getOpponentCharacter(playerNumber).hp;

		this.myPossibleActions = myPossibleActions;
		this.oppPossibleActions = oppPossibleActions;

		selfRootNode = new McNode(null, null, myPossibleActions);
		selfRootNode.expand();


		oppRootNode = new McNode(null, null, oppPossibleActions);
		oppRootNode.expand();
	}

	private void uct() {
		McNode selfLeafNode = selfRootNode.getLeafUCB();
		Deque<Action> selfActions = selfLeafNode.traceActions();

		McNode oppLeafNode = oppRootNode.getLeafUCB();
		Deque<Action> oppActions = oppLeafNode.traceActions();
// 		Deque<Action> oppActions = new ArrayDeque<Action>();

		double score = playout(selfActions, oppActions);

		selfLeafNode.update(score);
		oppLeafNode.update(-score);
	}

	private double playout(Deque<Action> selfActions, Deque<Action> oppActions) {
		while (selfActions.size() < SIMULATION_ACTS) {
			selfActions.add(myPossibleActions.get(rand.nextInt(myPossibleActions.size())));
		}

		while (oppActions.size() < SIMULATION_ACTS) {
			oppActions.add(oppPossibleActions.get(rand.nextInt(oppPossibleActions.size())));
		}

		FrameData simulatedFrame = simulator.simulate(fData, playerNumber, selfActions, oppActions, SIMULATION_TIME);
		playoutCount++;

		return getScore(simulatedFrame);
	}

	// basic hp diff
	private double getScore(FrameData frameData) {
		CharacterData myCharacter = frameData.getMyCharacter(this.playerNumber);
		CharacterData oppCharacter = frameData.getOpponentCharacter(this.playerNumber);

		int hpDiff = myCharacter.hp - oppCharacter.hp - (this.myOriHP - this.oppOriHP);
		// double eDiff = myCharacter.energy - oppCharacter.energy;
		return hpDiff; // + eDiff/10;
	}

	public Action getBestAction() {
		long sTime = System.nanoTime();
		while (System.nanoTime() - sTime <= availableTime) {
			uct();
		}
		System.out.println("PLAYOUT: " + this.playoutCount);
		return getBestScoreAction();
	}

	private Action getBestScoreAction() {
		double bestScore = -999999 ;
		Action bestAction = null;
		for (McNode node : this.selfRootNode.children) {
			node.aggScore = (1.0*node.score)/node.visit;
			if (node.aggScore > bestScore) {
				bestScore = node.aggScore;
				bestAction = node.selectedAction;
			}
		}

		return bestAction;
	}
}
