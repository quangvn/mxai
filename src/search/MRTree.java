package search;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Random;

import enumerate.Action;
import memory.NeuralMemory;
import memory.TriGram;
import simulator.Simulator;
import structs.CharacterData;
import structs.FrameData;
import structs.GameData;

class MRNode {
	int visit;
	double score;
	double aggScore;
	double ucb;

	List<MRNode> children;

	MRNode parent;
	int depth;

	Action selectedAction;

	MRNode() {
		parent = null;
		depth = 0;
		this.visit = 1;
	}

	MRNode(MRNode parent, Action selectedAction) {
		this.parent = parent;
		this.selectedAction = selectedAction;
		this.depth = parent.depth + 1;
		this.visit = 1;
		this.setUCB();
	}

	void expand(int noActs) {
		List<Action> actions = MRTree.tg.getFreqActions(this.selectedAction.ordinal(), noActs);

		assert(actions.size() > 0);

		this.children = new ArrayList<MRNode>();
		for (Action a : actions) {
			this.children.add(new MRNode(this, a));
		}
	}

	void expand(List<Action> actions) {
		this.children = new ArrayList<MRNode>();
		for (Action a : actions) {
			this.children.add(new MRNode(this, a));
		}
	}

	void expand() {
		this.expand(3);
	}

	double getUCB() {
		return 1.0*this.score/this.visit + MRTree.UCB_C * Math.sqrt((2 * Math.log(parent.visit)) / this.visit);
	}

	void setUCB() {
		 if (this.visit < 3) {
       this.ucb = 9999 + MRTree.rand.nextInt(50);
     } else {
       this.ucb = this.getUCB();
     }
	}

	MRNode selectChildUCB() {
		MRNode selectedNode = null;
    double bestUcb = -99999;

    for (MRNode child : this.children) {
      if (bestUcb < child.ucb) {
        selectedNode = child;
        bestUcb = child.ucb;
      }
    }

    assert(selectedNode != null);
    return selectedNode;
	}

	/*
	 * return current node if is leaf and depth = max depth
	 * otherwise expand and return a child
	 */
	MRNode getLeafUCB() {
		if (this.depth == MRTree.UCT_TREE_DEPTH) {
			return this;
		}

		if (this.children == null) {
			this.expand();
		}

		assert(this.children.size() > 0);
		return this.selectChildUCB().getLeafUCB();
	}

	Deque<Action> traceActions() {
		ArrayDeque<Action> actions = new ArrayDeque<Action>();
		MRNode curNode = this;
		while (curNode.parent != null) {
			actions.add(curNode.selectedAction);
			curNode = curNode.parent;
		}
		return actions;
	}

	void update(double score) {
		this.score += score;
		this.visit ++;

		if (this.parent != null) {
			this.setUCB();
			this.parent.update(score);
		}
	}
}

public class MRTree {
	MRNode selfRootNode;
	MRNode oppRootNode;

	boolean playerNumber;
	FrameData fData;
	GameData gData;
	Simulator simulator;

	int myOriHP;
	int oppOriHP;

  private List<Action> myPossibleActions;
  private List<Action> oppPossibleActions;

	int playoutCount;

	static int SIMULATION_TIME = 60;
	static int SIMULATION_ACTS = 5;
	static int UCT_TREE_DEPTH = 3;
	static double UCB_C = 4;
	static int UCT_B = 3;
	static final Random rand = new Random();

	public static TriGram tg;
	public static NeuralMemory hm;

	private static Action[] actionAir =
      new Action[] {
      		Action.AIR_GUARD,
      		Action.AIR_A,
      		Action.AIR_B,
      		Action.AIR_DA,
      		Action.AIR_DB,
          Action.AIR_FA,
          Action.AIR_FB,
          Action.AIR_UA,
          Action.AIR_UB,
          Action.AIR_D_DF_FA,
          Action.AIR_D_DF_FB,
          Action.AIR_F_D_DFA,
          Action.AIR_F_D_DFB,
          Action.AIR_D_DB_BA,
          Action.AIR_D_DB_BB
       		};

  private static Action[] actionGround =
      new Action[] {
      		Action.STAND_D_DB_BA,
      		Action.BACK_STEP,
      		Action.FORWARD_WALK,
      		Action.DASH,
          Action.JUMP,
          Action.FOR_JUMP,
          Action.BACK_JUMP,
          Action.STAND_GUARD,
          Action.CROUCH_GUARD,
          Action.THROW_A,
          Action.THROW_B,
          Action.STAND_A,
          Action.STAND_B,
          Action.CROUCH_A,
          Action.CROUCH_B,
          Action.STAND_FA,
          Action.STAND_FB,
          Action.CROUCH_FA,
          Action.CROUCH_FB,
          Action.STAND_D_DF_FA,
          Action.STAND_D_DF_FB,
          Action.STAND_F_D_DFA,
          Action.STAND_F_D_DFB,
          Action.STAND_D_DB_BB};

  private static Action spSkill = Action.STAND_D_DF_FC;

	Action[] actions;

	long availableTime = 15000000; // time in nano-second

	public MRTree(FrameData frameData, GameData gameData, boolean playerNumber, List<Action> myPossibleActions, List<Action> oppPossibleActions) {
		this.fData = frameData;
		this.gData = gameData;
		this.simulator = new Simulator(gameData);
		this.playerNumber = playerNumber;
		this.playoutCount = 0;

		this.myOriHP = frameData.getMyCharacter(playerNumber).hp;
		this.oppOriHP = frameData.getOpponentCharacter(playerNumber).hp;

		this.myPossibleActions = myPossibleActions;
		this.oppPossibleActions = oppPossibleActions;

		selfRootNode = new MRNode();
		List<Action> myActions = hm.getBestActions(frameData, playerNumber, UCT_B);
		selfRootNode.expand(myActions);


		oppRootNode = new MRNode();
		List<Action> oppActions = hm.getBestActions(frameData, !playerNumber, UCT_B-3);
    if (frameData.getOpponentCharacter(playerNumber) != null) {
    	oppRootNode.selectedAction = frameData.getOpponentCharacter(playerNumber).action;
    } else {
    	oppRootNode.selectedAction = actions[0];
		}
		oppActions.addAll(tg.getFreqActions(oppRootNode.selectedAction.ordinal(), 3));
		oppRootNode.expand(oppActions);
	}

	private void uct() {
		MRNode selfLeafNode = selfRootNode.getLeafUCB();
		Deque<Action> selfActions = selfLeafNode.traceActions();

		MRNode oppLeafNode = oppRootNode.getLeafUCB();
		Deque<Action> oppActions = oppLeafNode.traceActions();

		double score = playout(fData, selfActions, oppActions, SIMULATION_TIME);

		selfLeafNode.update(score);
		oppLeafNode.update(-score);
	}

	private double playout(FrameData fData, Deque<Action> selfActions, Deque<Action> oppActions, int SIMULATION_TIME) {
		while (selfActions.size() < SIMULATION_ACTS) {
			selfActions.add(myPossibleActions.get(rand.nextInt(myPossibleActions.size())));
		}

		while (oppActions.size() < SIMULATION_ACTS) {
			/*
			if (oppActions.size() == 1) {
				a = tg.getFreqActions(oppActions.peekFirst().ordinal(), randSize);
			} else {
				a = tg.getFreqActions(oppActions.peekFirst().ordinal(), randSize);
			}
			*/
			// oppActions.add(a.get(rand.nextInt(randSize)));
			// oppActions.add(a.get(0));
			oppActions.add(oppPossibleActions.get(rand.nextInt(oppPossibleActions.size())));
		}

		FrameData simulatedFrame = simulator.simulate(fData, playerNumber, selfActions, oppActions, SIMULATION_TIME);
		playoutCount++;

		return getScore(simulatedFrame);
	}

	// basic hp diff
	private double getScore(FrameData fData) {
		CharacterData myCharacter = fData.getMyCharacter(this.playerNumber);
		CharacterData oppCharacter = fData.getOpponentCharacter(this.playerNumber);

		int hpDiff = myCharacter.hp - oppCharacter.hp - (this.myOriHP - this.oppOriHP);
		double eDiff = myCharacter.energy - oppCharacter.energy;
		return hpDiff + eDiff/40; //  + hm.getBestActionScore(fData, playerNumber)/2;
	}

	public Action getBestAction() {
		long sTime = System.nanoTime();
		while (System.nanoTime() - sTime <= availableTime) {
			uct();
		}
		// System.out.println("PLAYOUT: " + this.playoutCount);
		return getBestScoreAction();
	}

	private Action getBestScoreAction() {
		double bestScore = -999999 ;
		Action bestAction = null;
		for (MRNode node : this.selfRootNode.children) {
			node.aggScore = (1.0*node.score)/node.visit;
			if (node.aggScore > bestScore) {
				bestScore = node.aggScore;
				bestAction = node.selectedAction;
			}
		}

		int[] actIndex = new int[UCT_B];
		double[] actScore = new double[UCT_B];
		for (int i=0; i<UCT_B; i++) {
			actIndex[i] = selfRootNode.children.get(i).selectedAction.ordinal();
			actScore[i] = 3*(support.Mathf.sigmoid(selfRootNode.children.get(i).aggScore/40)-0.5);
		}
		return bestAction;
	}
}
