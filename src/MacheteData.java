/*
 * Machete
 * Version 0.989
 * Last update on 15 August 2015
 * Made using FightingICE version 1.22
 */

import java.util.Date;

import commandcenter.CommandCenter;
import enumerate.State;
import gameInterface.AIInterface;
import simulator.Simulator;
import structs.CharacterData;
import structs.FrameData;
import structs.GameData;
import structs.Key;
import support.DataExtractor;


public class MacheteData implements AIInterface {

	private Key inputKey;
	private boolean player;
	private FrameData frameData;
	private CommandCenter cc;
	private Simulator simulator;
	private GameData gd;

	private int distance;
	private int energy;
	private CharacterData opp;
	private CharacterData my;
	private boolean isGameJustStarted;
	private int xDifference;


	private DataExtractor de;
	Thread thread;

	@Override
	public void close() {
		// wait for data extractor to finish
		de.stop();
		try {
			thread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public String getCharacter() {
		// Select the player ZEN as per competition rules
		return CHARACTER_ZEN;
	}

	@Override
	public void getInformation(FrameData frameData) {
		// Load the frame data every time getInformation gets called
		this.frameData = frameData;

		System.out.println(frameData.getRemainingTime());

		// output the frameData
		if (frameData.getP1() != null) {
			de.printFrameData(frameData, frameData.getP1(), frameData.getP2());
		}
	}

	@Override
	public int initialize(GameData arg0, boolean player) {
		// Initialize the global variables at the start of the round
		inputKey = new Key();
		this.player = player;
		frameData = new FrameData();
		cc = new CommandCenter();
		gd = arg0;
		simulator = gd.getSimulator();
		isGameJustStarted = true;
		xDifference = -300;

		// new log thread
		Date d = new Date();
		String filename = "data/aiData/log_" + gd.getPlayerOneCharacterName() + "_" + gd.getPlayerTwoCharacterName() + ".csv";
		de = new DataExtractor(filename, player, gd.getStageXMax(), gd.getStageYMax());
		thread = new Thread(de);
		System.out.println("Starting data extractor thread...");
		thread.start();
		return 0;
	}

	@Override
	public Key input() {
		// The input is set up to the global variable inputKey
		// which is modified in the processing part
		return inputKey;
	}

	@Override
	public void processing() {
		// First we check whether we are at the end of the round
		if(!frameData.getEmptyFlag() && frameData.getRemainingTime()>0){
			// Simulate the delay and look ahead 2 frames. The simulator class exists already in FightingICE
			if (!isGameJustStarted)
				frameData = simulator.simulate(frameData, this.player, null, null, 17);
			else
				isGameJustStarted = false; //if the game just started, no point on simulating
			cc.setFrameData(frameData, player);
			distance = cc.getDistanceX();
			energy = frameData.getMyCharacter(player).getEnergy();
			my = cc.getMyCharacter();
			opp = cc.getEnemyCharacter();
			xDifference = my.x - opp.x;

			if (cc.getskillFlag()) {
				// If there is a previous "command" still in execution, then keep doing it
				inputKey = cc.getSkillKey();
			}
			else {
				// We empty the keys and cancel skill just in case
				inputKey.empty();
				cc.skillCancel();
				// Following is the brain of the reflex agent. It determines distance to the enemy
				// and the energy of our agent and then it performs an action
				if ((opp.energy >= 300) && ((my.hp - opp.hp) <= 300))
					cc.commandCall("FOR_JUMP _B B B");
					// if the opp has 300 of energy, it is dangerous, so better jump!!
					// if the health difference is high we are dominating so we are fearless :)
				else if (!my.state.equals(State.AIR) && !my.state.equals(State.DOWN)) { //if not in air
					if ((distance > 150)) {
						cc.commandCall("FOR_JUMP"); //If its too far, then jump to get closer fast
					}
					else if (energy >= 300)
						cc.commandCall("STAND_D_DF_FC"); //High energy projectile
					else if ((distance > 100) && (energy >= 50))
						cc.commandCall("STAND_D_DB_BB"); //Perform a slide kick
					else if (opp.state.equals(State.AIR)) //if enemy on Air
						cc.commandCall("STAND_F_D_DFA"); //Perform a big punch
					else if (distance > 100)
						cc.commandCall("6 6 6"); // Perform a quick dash to get closer
					else
						cc.commandCall("B"); //Perform a kick in all other cases, introduces randomness
				}
				else if ((distance <= 150) && (my.state.equals(State.AIR) || my.state.equals(State.DOWN))
						&& (((gd.getStageXMax() - my.x)>=200) || (xDifference > 0))
						&& ((my.x >=200) || xDifference < 0)) { //Conditions to handle game corners
					if (energy >= 5)
						cc.commandCall("AIR_DB"); // Perform air down kick when in air
					else
						cc.commandCall("B"); //Perform a kick in all other cases, introduces randomness
				}
				else
					cc.commandCall("B"); //Perform a kick in all other cases, introduces randomness
			}
		}
		else isGameJustStarted = true;
	}

	 public boolean canProcessing() {
        return !frameData.getEmptyFlag() && frameData.getRemainingTime() > 0 && !cc.getskillFlag();
    }
}
