import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Vector;

import commandcenter.CommandCenter;
import enumerate.Action;
import enumerate.State;
import fighting.Attack;
import gameInterface.AIInterface;
import memory.HashMemory;
import memory.TriGram;
import search.MPTree;
import simulator.Simulator;
import structs.CharacterData;
import structs.FrameData;
import structs.GameData;
import structs.Key;
import structs.MotionData;

/**
 * Minimax-liked tree search
 *
 * @author Quang Vu
 */
public class MPAi implements AIInterface {

  private Simulator simulator;
  private Key key;
  private CommandCenter commandCenter;
  private boolean playerNumber;
  private GameData gameData;

  /** Main FrameData */
  private FrameData frameData;

  /** Data with FRAME_AHEAD frames ahead of FrameData */
  private FrameData simulatorAheadFrameData;

  private List<Action> myActions;
  private List<Action> oppActions;

  private Deque<Action> myRecentActions;
  private Deque<Action> oppRecentActions;

  private Vector<MotionData> myMotion;
  private Vector<MotionData> oppMotion;

  private Attack myLastAttack;
  private Attack oppLastAttack;

  private CharacterData myCharacter;
  private CharacterData oppCharacter;

  /** Number of adjusted frames (following the same recipe in JerryMizunoAI) */
  private static final int FRAME_AHEAD = 14;

	private static Action[] actionAir =
      new Action[] {
      		Action.AIR_GUARD,
      		Action.AIR_A,
      		Action.AIR_B,
      		Action.AIR_DA,
      		Action.AIR_DB,
          Action.AIR_FA,
          Action.AIR_FB,
          Action.AIR_UA,
          Action.AIR_UB,
          Action.AIR_D_DF_FA,
          Action.AIR_D_DF_FB,
          Action.AIR_F_D_DFA,
          Action.AIR_F_D_DFB,
          Action.AIR_D_DB_BA,
          Action.AIR_D_DB_BB
       		};

  private static Action[] actionGround =
      new Action[] {
      		Action.STAND_D_DB_BA,
      		Action.BACK_STEP,
      		Action.FORWARD_WALK,
      		Action.DASH,
          Action.JUMP,
          Action.FOR_JUMP,
          Action.BACK_JUMP,
          Action.STAND_GUARD,
          Action.CROUCH_GUARD,
          Action.THROW_A,
          Action.THROW_B,
          Action.STAND_A,
          Action.STAND_B,
          Action.CROUCH_A,
          Action.CROUCH_B,
          Action.STAND_FA,
          Action.STAND_FB,
          Action.CROUCH_FA,
          Action.CROUCH_FB,
          Action.STAND_D_DF_FA,
          Action.STAND_D_DF_FB,
          Action.STAND_F_D_DFA,
          Action.STAND_F_D_DFB,
          Action.STAND_D_DB_BB};

  private static Action spSkill = Action.STAND_D_DF_FC;

  /** True if in debug mode, which will output related log */
  public static final boolean DEBUG_MODE = true;

  /** True if output log match result to file */
  public static final boolean LOG_MATCH_MODE = true;
	private static final String LOG_MATCH_RESULT_FILE_NAME = "data/aiData/log_res.csv";

  /** initialization flag */
  private static boolean initialized = false;

  // log result
  private int lastRound;
  private int myScore;
  private int oppScore;

  @Override
  public void close() {
//  	MPTree.lm.updateMatch();
  	MPTree.tg.store();
  	MPTree.hm.store();
  	printResult();
  }

  @Override
  public String getCharacter() {
    return CHARACTER_ZEN;
  }

  @Override
  public void getInformation(FrameData frameData) {
  	if (!MPAi.initialized) return;
  	if (frameData.getP1() == null) return;

    this.frameData = frameData;
    this.commandCenter.setFrameData(this.frameData, playerNumber);

    this.myCharacter = this.frameData.getMyCharacter(playerNumber);
    this.oppCharacter = this.frameData.getOpponentCharacter(playerNumber);

    if (this.myCharacter.action == null) return;

    if (this.myRecentActions.size() < 3) {
    	if (isNewAttack(myLastAttack, this.myCharacter.attack)) {
    		this.myRecentActions.add(this.myCharacter.action);
    	}
    } else if (this.myCharacter.action.ordinal() != this.myRecentActions.peekLast().ordinal() ||
    		isNewAttack(myLastAttack, this.myCharacter.attack)) {
    	MPTree.tg.update(this.myRecentActions.peekLast().ordinal(), this.myCharacter.action.ordinal());
    	this.myRecentActions.add(this.myCharacter.action);
    	this.myRecentActions.poll();
    }

    if (this.oppRecentActions.size() < 3) {
    	if (isNewAttack(oppLastAttack, this.oppCharacter.attack)) {
    		this.oppRecentActions.add(this.oppCharacter.action);
    	}
    } else if (this.oppCharacter.action.ordinal() != this.oppRecentActions.peekLast().ordinal() ||
    		isNewAttack(oppLastAttack, this.oppCharacter.attack)) {
    	MPTree.tg.update(this.oppRecentActions.peekLast().ordinal(), this.oppCharacter.action.ordinal());
    	this.oppRecentActions.add(this.oppCharacter.action);
    	this.oppRecentActions.poll();
    }

    if (frameData.getRound() != this.lastRound) {
    	printResult();
    	this.lastRound = frameData.getRound();
    }
    this.myScore = frameData.getMyCharacter(playerNumber).hp;
    this.oppScore = frameData.getOpponentCharacter(playerNumber).hp;

//    MPTree.lm.updateIngame(frameData, playerNumber);
  }

  @Override
  public int initialize(GameData gameData, boolean playerNumber) {
    this.playerNumber = playerNumber;
    this.gameData = gameData;

    this.key = new Key();
    this.frameData = new FrameData();
    this.commandCenter = new CommandCenter();

    simulator = gameData.getSimulator();

		myMotion = this.playerNumber ? gameData.getPlayerOneMotion() : gameData.getPlayerTwoMotion();
    oppMotion = this.playerNumber ? gameData.getPlayerTwoMotion() : gameData.getPlayerOneMotion();

    this.myRecentActions = new ArrayDeque<Action>();
    this.oppRecentActions = new ArrayDeque<Action>();

//    MPTree.lm = new LayeredMemory();
//    MPTree.lm.load();

    MPTree.tg = new TriGram();
    MPTree.tg.load();

    MPTree.hm = new HashMemory();
    MPTree.hm.load();

    MPAi.initialized = true;
    this.lastRound = 0;

 		return 0;
  }

  @Override
  public Key input() {
    return key;
  }

  @Override
  public void processing() {

    if (canProcessing()) {
      if (commandCenter.getskillFlag()) {
        key = commandCenter.getSkillKey();
      } else {
        key.empty();
        commandCenter.skillCancel();

        // System.out.println("Distance: " + Math.abs(myCharacter.left-oppCharacter.left) );
        prepare(); // Some preparation for MCTS
        MPTree searchTree = new MPTree(simulatorAheadFrameData, gameData, playerNumber, myActions, oppActions);

        Action bestAction = searchTree.getBestAction(); // Perform search
        commandCenter.commandCall(bestAction.name()); // Perform an action selected by MCTS
      }
    }
  }

  /**
   * Determine whether or not the AI can perform an action
   *
   * @return whether or not the AI can perform an action
   */
  public boolean canProcessing() {
    return !frameData.getEmptyFlag() && frameData.getRemainingTime() > 0;
  }

  /**
   * Some preparation
   * Perform the process for obtaining FrameData with 14 frames ahead
   */
  public void prepare() {
    simulatorAheadFrameData = simulator.simulate(frameData, playerNumber, null, null, FRAME_AHEAD);

    setMyAction();
    setOppAction();
  }

  private boolean isNewAttack(Attack a1, Attack a2) {
  	if (a1 == null || a2 == null) return true;
  	if (a1.getNowFrame()  >= a2.getNowFrame()) {
  		return true;
  	}
  	return false;
  }

  public void setMyAction() {
    myActions = new ArrayList<Action>();

    int energy = myCharacter.getEnergy();

    if (myCharacter.getState() == State.AIR) {
      for (int i = 0; i < actionAir.length; i++) {
        if (Math.abs(myMotion.elementAt(Action.valueOf(actionAir[i].name()).ordinal())
            .getAttackStartAddEnergy()) <= energy) {
          myActions.add(actionAir[i]);
        }
      }
    } else {
      if (Math.abs(myMotion.elementAt(Action.valueOf(spSkill.name()).ordinal())
          .getAttackStartAddEnergy()) <= energy) {
        myActions.add(spSkill);
        myActions.add(spSkill);
      }

      for (int i = 0; i < actionGround.length; i++) {
        if (Math.abs(myMotion.elementAt(Action.valueOf(actionGround[i].name()).ordinal())
            .getAttackStartAddEnergy()) <= energy) {
          myActions.add(actionGround[i]);
        }
      }

      if (Math.abs(oppMotion.elementAt(Action.valueOf(spSkill.name()).ordinal())
          .getAttackStartAddEnergy()) <= energy) {
      	myActions.add(spSkill);
      	myActions.add(spSkill);
      }
    }

  }

  public void setOppAction() {
    oppActions = new ArrayList<Action>();

    int energy = oppCharacter.getEnergy();

    if (oppCharacter.getState() == State.AIR) {
      for (int i = 0; i < actionAir.length; i++) {
        if (Math.abs(oppMotion.elementAt(Action.valueOf(actionAir[i].name()).ordinal())
            .getAttackStartAddEnergy()) <= energy) {
          oppActions.add(actionAir[i]);
        }
      }
    } else {
      if (Math.abs(oppMotion.elementAt(Action.valueOf(spSkill.name()).ordinal())
          .getAttackStartAddEnergy()) <= energy) {
        oppActions.add(spSkill);
        oppActions.add(spSkill);
      }

      for (int i = 0; i < actionGround.length; i++) {
        if (Math.abs(oppMotion.elementAt(Action.valueOf(actionGround[i].name()).ordinal())
            .getAttackStartAddEnergy()) <= energy) {
          oppActions.add(actionGround[i]);
        }
      }

      if (Math.abs(oppMotion.elementAt(Action.valueOf(spSkill.name()).ordinal())
          .getAttackStartAddEnergy()) <= energy) {
        oppActions.add(spSkill);
        oppActions.add(spSkill);
      }
    }
  }

	private void printResult() {
		try {
			PrintWriter pw = new PrintWriter(new BufferedWriter (new FileWriter(LOG_MATCH_RESULT_FILE_NAME, true)));
			StringBuilder sb = new StringBuilder();
			sb.append(this.oppScore).append(",");
			sb.append(this.myScore).append(",");
			sb.append((-this.myScore*1000)/(-this.oppScore-this.myScore+1)).append(",");
			sb.append((-this.oppScore*1000)/(-this.oppScore-this.myScore+1));

			pw.println(sb.toString());
			pw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}