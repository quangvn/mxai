

import enumerate.Action;
import enumerate.State;
import gameInterface.AIInterface;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.LinkedList;
import java.util.Vector;

import simulator.Simulator;
import structs.CharacterData;
import structs.FrameData;
import structs.GameData;
import structs.Key;
import structs.MotionData;
import support.DataExtractor;
import support.NeuralNetwork;
import commandcenter.CommandCenter;

/**
 * Minimax-liked tree search
 *
 * @author Quang Vu
 */
public class MxAi2 implements AIInterface {

  private Simulator simulator;
  private Key key;
  private CommandCenter commandCenter;
  private boolean playerNumber;
  private GameData gameData;

  /** Main FrameData */
  private FrameData frameData;

  /** Data with FRAME_AHEAD frames ahead of FrameData */
  private FrameData simulatorAheadFrameData;

  /** Number of adjusted frames (following the same recipe in JerryMizunoAI) */
  private static final int FRAME_AHEAD = 14;

  private MxNode rootNode;

  // neural network
  NeuralNetwork nn;
  
  
  @Override
  public void close() {
  }

  @Override
  public String getCharacter() {
    return CHARACTER_ZEN;
  }

  @Override
  public void getInformation(FrameData frameData) {
    this.frameData = frameData;
    this.commandCenter.setFrameData(this.frameData, playerNumber);
  }

  @Override
  public int initialize(GameData gameData, boolean playerNumber) {
    this.playerNumber = playerNumber;
    this.gameData = gameData;

    this.key = new Key();
    this.frameData = new FrameData();
    this.commandCenter = new CommandCenter();

    simulator = gameData.getSimulator();
    
		// loading weights
		nn = new NeuralNetwork();
		nn.load("data/aiData/old_weights.txt");
		
		
		MxNode.myMotion = this.playerNumber ? gameData.getPlayerOneMotion() : gameData.getPlayerTwoMotion();
    MxNode.oppMotion = this.playerNumber ? gameData.getPlayerTwoMotion() : gameData.getPlayerOneMotion();
    
    
 		return 0;
  }

  @Override
  public Key input() {
    return key;
  }

  @Override
  public void processing() {

    if (canProcessing()) {
      if (commandCenter.getskillFlag()) {
        key = commandCenter.getSkillKey();
      } else {
        key.empty();
        commandCenter.skillCancel();

        long st = System.currentTimeMillis();

        prepare(); // Some preparation for MCTS
        rootNode =
            new MxNode(simulatorAheadFrameData, null, gameData, playerNumber,
                commandCenter, nn);

        Action bestAction = rootNode.getBestAction(); // Perform search
        commandCenter.commandCall(bestAction.name()); // Perform an action selected by MCTS
      }
    }
  }

  /**
   * Determine whether or not the AI can perform an action
   *
   * @return whether or not the AI can perform an action
   */
  public boolean canProcessing() {
    return !frameData.getEmptyFlag() && frameData.getRemainingTime() > 0;
  }

  /**
   * Some preparation
   * Perform the process for obtaining FrameData with 14 frames ahead
   */
  public void prepare() {
    simulatorAheadFrameData = simulator.simulate(frameData, playerNumber, null, null, FRAME_AHEAD);
  }
}